#ifndef FILEPROCESSOR_H
#define FILEPROCESSOR_H

#include <stdbool.h>
#include "transport.h"

#define MSS 1450

void processFile();
char *getFileContentByFileName(char fileName[]);
char *getFileContentByFilePointer(FILE *fp);
long findSizeByFilePointer(FILE* fp);
long findSizeByFileName(char file_name[]);
bool exists(const char *);
void saveFile(char *, Response*);
#endif
