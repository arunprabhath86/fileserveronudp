CC=gcc
LIBS=-lpthread -lstdc++
CFLAGS=-w
SOURCES_COMMON=$(wildcard src/common/*.c)
SOURCES_SERVER=$(wildcard src/server/*.c)
SOURCES_CLIENT=$(wildcard src/client/*.c)
OBJECTS_COMMON=$(patsubst %.c, %.o, $(SOURCES_COMMON))
OBJECTS_SERVER=$(patsubst %.c, %.o, $(SOURCES_SERVER))
OBJECTS_CLIENT=$(patsubst %.c, %.o, $(SOURCES_CLIENT))
EXECUTABLE_SERVER=bin/rudpserver
EXECUTABLE_CLIENT=bin/rudpclient

all: build $(EXECUTABLE_SERVER) $(EXECUTABLE_CLIENT)

$(EXECUTABLE_SERVER):  $(OBJECTS_SERVER) $(OBJECTS_COMMON)
	$(CC) $(CFLAGS) $(OBJECTS_SERVER) $(OBJECTS_COMMON) $(LIBS) -o $@

$(EXECUTABLE_CLIENT):  $(OBJECTS_CLIENT) $(OBJECTS_COMMON)
	$(CC) $(CFLAGS) $(OBJECTS_CLIENT) $(OBJECTS_COMMON) $(LIBS) -o $@

$(OBJECTS_SERVER): src/server/%.o : src/server/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

$(OBJECTS_CLIENT): src/client/%.o : src/client/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

$(OBJECTS_COMMON): src/common/%.o : src/common/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

build:
	mkdir -p bin
clean:
	rm -rf $(EXECUTABLE_SERVER) $(EXECUTABLE_CLIENT) $(OBJECTS_COMMON) $(OBJECTS_SERVER) $(OBJECTS_CLIENT) bin
