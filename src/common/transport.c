
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <getopt.h>
#include <netdb.h>
#include <errno.h>


#include "transport.h"
#include "udpsegment.h"
#include "log.h"

struct timeval timeoutInterval, devRTT, estimatedRTT;
static int sequenceNumber = 0;




void sendUsingSlidingWindow(UDPSegment *udpSegments[], int totalSegments, int *currentSockFd, struct sockaddr_in *otherEnd, int windowSize);

void createSegments(char *, long size, int *currentSockFd, struct sockaddr_in *otherEnd, int advWindowSize, int type, int error);

struct timeval calculateTimeout(struct timeval t1, struct timeval t2) {

  double alpha = 0.125;
  double beta = 0.25;
  struct timeval sampleRTT;

  sampleRTT.tv_sec = t2.tv_sec - t1.tv_sec;
  sampleRTT.tv_usec = t2.tv_usec - t1.tv_usec;

  estimatedRTT.tv_sec = ((1 - alpha) * estimatedRTT.tv_sec + (alpha * sampleRTT.tv_sec));
  estimatedRTT.tv_usec = ((1 - alpha) * estimatedRTT.tv_usec + (alpha * sampleRTT.tv_usec));
  devRTT.tv_sec = ((1 - beta) * devRTT.tv_sec + beta * (abs(sampleRTT.tv_sec - estimatedRTT.tv_sec)));
  devRTT.tv_usec = ((1 - beta) * devRTT.tv_usec + beta * (abs(sampleRTT.tv_usec - estimatedRTT.tv_usec)));
  timeoutInterval.tv_sec = estimatedRTT.tv_sec + 4 * devRTT.tv_sec;
  timeoutInterval.tv_usec = estimatedRTT.tv_usec + 4 * devRTT.tv_usec;

  return timeoutInterval;
}

void reliableSend(char *input, long fileSize, int *currentSockFd, struct sockaddr_in *otherEnd, int advWindowSize, int type, int error) {
  createSegments(input, fileSize, currentSockFd, otherEnd, advWindowSize, type, error);
}

UDPSegment* setSegmentHeader(int seqNo, int ackNo, int flag, char *datagram, int error) {
	UDPSegment *udpData = (UDPSegment *)malloc(sizeof(UDPSegment));
	udpData->seqNo = seqNo;
	udpData->ackNo = ackNo;
	udpData->flag = flag;
  udpData->error = error;
	strcpy(udpData->data, datagram);
	return udpData;
}

UDPSegment* receiveAck(int *currentSockFd, struct sockaddr_in *otherEnd) {
	UDPSegment *ack = (UDPSegment *)malloc(sizeof(UDPSegment));
  socklen_t len = sizeof (*otherEnd);
	int no = recvfrom(*currentSockFd, ack, sizeof(*ack), 0,
                    (struct sockaddr *) otherEnd,
                    &len);
	if(no<0) {
		log_error("There is some problem in receiving the segment Error: %d", errno);
	} else {
    log_info("Received Acknowledgement %d for sequence number %d ", ack->ackNo, ack->seqNo);
  }
	return ack;
}


void sendSegment(UDPSegment *seg, int *currentSockFd, struct sockaddr_in *otherEnd) {
  log_debug("Sending Segment with sequence number: %d ", seg->seqNo);
	int no = sendto(*currentSockFd, seg, sizeof(UDPSegment), 0,
                  (struct sockaddr *) otherEnd, sizeof(*otherEnd));
	if(no < 0) {
		log_error("There is some problem in sending the segment! Error No: %d", errno);
	}
}

void createSegments(char *fileContent, long fileSize, int *currentSockFd, struct sockaddr_in *otherEnd, int windowSize, int type, int error) {

	int noOfSegments = fileSize / MSS;
  log_debug("Number of segments : %d", noOfSegments);
	char seg[MSS];
	int seqNo = 0;
	int ackNo = 1;
	int ackFlag = 0;
	int i = 0, j, k;
  bzero(seg, MSS);
	int senderBufferLen = noOfSegments + 1;
	UDPSegment **senderBuffer =  malloc(senderBufferLen * sizeof(UDPSegment));
	for( j = 0; j < noOfSegments; j++) {
		for(i = j*MSS, k=0; i<(j+1) * MSS && k < MSS; i++, k++) {
			seg[k] = fileContent[i];
		}
		senderBuffer[j] = setSegmentHeader(seqNo, ackNo, type, seg, error);
		seqNo++;
	}
	int rem = fileSize % MSS;
  log_debug("Remaining data length : %d", rem);
  if (i == 0) {
  	for( int s=0; s<rem; s++) {
  		seg[s] = fileContent[s];
  	}
  } else {
    for( int s=0; s<rem; s++) {
  		seg[s] = fileContent[++i];
  	}
  }
  senderBuffer[j] = setSegmentHeader(seqNo, ackNo, ackFlag, seg, error);
	sendUsingSlidingWindow(senderBuffer, senderBufferLen, currentSockFd, otherEnd, windowSize);
}

void sendAck(UDPSegment *ack, int *sockfd, struct sockaddr_in *remoteAddr, int *len) {
  if (ack) {
    UDPSegment *acknowledge =  malloc(sizeof(UDPSegment));
  	acknowledge->ackNo = ack->seqNo+1;
  	acknowledge->seqNo = ack->seqNo;
  	acknowledge->flag = ACK;
    acknowledge->error = 0;
    acknowledge->data[0] = '\0';
  	log_debug("Sending Acknowledgement Number (%d) for sequence number (%d)", acknowledge->ackNo, ack->seqNo);
  	int noBytesSent = sendto(*sockfd , acknowledge, sizeof(*acknowledge), 0, (struct sockaddr *)remoteAddr, len);
    if (noBytesSent < 0) {
  		log_error("There is problem while sending acknowledgement! ");
  	}
    free(acknowledge);
  }
}

Response * readResponse(int windowSize, int *sockfd, struct sockaddr_in *remoteAddr) {
  UDPSegment **receiveBuffer =  malloc(30000 * sizeof(UDPSegment*));
	int nxtExpectedSeqNum=0;
	int receiveBufferInd=0;
	int winCount=0;
	int noOfSegInWin=windowSize/sizeof(UDPSegment*);
	int no=1;
	UDPSegment *segment =  malloc(sizeof(UDPSegment));;
	segment->flag= DATA;
	struct timeval tv;
	fd_set fds;
	int val=1;
  int fromlen = sizeof(*remoteAddr);
  int maxFd = (*sockfd) + 1;
  int curFd = *sockfd;
  Response *response = malloc(sizeof(Response *));
	while(segment->flag != COMMAND_OR_END) {
		no=1;
		bool outOfOrderFlag=false;
		while(winCount < noOfSegInWin && (segment->flag == DATA || segment->flag == FILE_HEADER)) {
			FD_ZERO(&fds);
			FD_CLR(curFd,&fds);
			FD_SET(curFd,&fds);
			tv.tv_sec = 0;
			tv.tv_usec =100000;
			val = select(maxFd , &fds, NULL, NULL, &tv);
			if(val == 0) {
				break;
			}
			if(val == -1) {
				log_error("There is some problem in receiving the segment!");
			}
			if(FD_ISSET(*sockfd, &fds) && val == 1) {
        segment =  malloc(sizeof(UDPSegment));;
        memset((char*)remoteAddr, 0, sizeof(struct sockaddr_in));
				no=recvfrom(*sockfd, segment, sizeof(*segment),0, (struct sockaddr *)remoteAddr, &fromlen);

        if (no <0) {
            log_debug("Error reading from other end Error No: %d", errno);
        }
				int seqNo = segment->seqNo;
        log_debug("Received Segment for the sequenceNumber: %d", seqNo);
				if(seqNo == nxtExpectedSeqNum && receiveBufferInd < 30000) {
					receiveBuffer[receiveBufferInd] = segment;
					nxtExpectedSeqNum = segment->seqNo+1;
					receiveBufferInd++;
					winCount++;
				} else {
					outOfOrderFlag=true;
				}
			}
		}

		if(outOfOrderFlag) {
      UDPSegment *ack =  malloc(sizeof(UDPSegment));
			if(receiveBufferInd==0) {
				for(int i = 0;i < 3; i++) {
					ack->seqNo = 0;
					ack->ackNo = 0;
					ack->flag = ACK;
          log_info("Sending Acknowledgement Number (%d) for sequence number (%d)", ack->ackNo, ack->seqNo);
					int no = sendto(*sockfd, ack, sizeof(*ack), 0, (struct sockaddr *)remoteAddr, sizeof(*remoteAddr));
					if (no<0) {
						log_error("There is problem while sending acknowledgement!");
					}
				}
			} else {
				for(int i = 0; i < 3; i++) {
					ack->seqNo = receiveBuffer[receiveBufferInd-1]->seqNo;
					ack->ackNo=receiveBuffer[receiveBufferInd-1]->ackNo;
					sendAck(ack, sockfd, remoteAddr, fromlen);
				}
			}
      free(ack);
		} else {
      if (receiveBufferInd - 1 >= 0) {
        sendAck(receiveBuffer[receiveBufferInd - 1], sockfd, remoteAddr, fromlen);
      }
		}
		winCount=0;
	}
  FD_CLR(curFd, &fds);
  response->receiveBuffer = receiveBuffer;
  response->length = receiveBufferInd;
  return response;
}






void sendUsingSlidingWindow(UDPSegment *udpSegments[], int totalSegments, int *currentSockFd, struct sockaddr_in *otherEnd, int windowSize) {
  int firstUnAck = 0, nxtSeqNo = 0, dupAckCnt;
  UDPSegment *ack;
  log_info("Total Segments to be sent: %d ", totalSegments);
  int cwnd =  1;
  int ssthresh = 64000;
  int segmentSize = sizeof(*udpSegments[0]);
  int noOfSegmentsInWin = windowSize / segmentSize;
  struct timeval t1, t2;
  log_info("Segments in window %d", noOfSegmentsInWin);
  int dropPercent = 60;
  int noOfPacketsToDrop = (dropPercent * noOfSegmentsInWin) / 100;
  log_info("No of packets to drop %d ", noOfPacketsToDrop);
  int packetsToDrop[noOfPacketsToDrop];
  estimatedRTT.tv_sec = 0;
  estimatedRTT.tv_usec = 0;
  devRTT.tv_sec = 0;
  devRTT.tv_usec = 0;
  timeoutInterval.tv_sec = 2;
  timeoutInterval.tv_usec = 0;
  fd_set fds;
  int val = 1;
  while(nxtSeqNo < totalSegments) {
    if(firstUnAck == 0 && nxtSeqNo == 0) {
      srand(time(NULL));
      for(int i = 0;i < noOfPacketsToDrop; i++) {
        packetsToDrop[i] = rand() % (noOfSegmentsInWin - 1) + 1;
      }
    }
    int minimumSize = (cwnd < noOfSegmentsInWin)? cwnd : noOfSegmentsInWin;
    gettimeofday(&t1, NULL);
    while(nxtSeqNo <= firstUnAck + minimumSize && nxtSeqNo < totalSegments) {
      if(nxtSeqNo == totalSegments - 1) {
        udpSegments[nxtSeqNo]->flag = COMMAND_OR_END; // To mark as end of the packets
      }
      bool flag=true;
      for(int i = 0; i < noOfPacketsToDrop; i++) {
        if(nxtSeqNo == packetsToDrop[i]) {
          flag=false;
        }
      }
      if(flag) {
        sendSegment(udpSegments[nxtSeqNo], currentSockFd, otherEnd);
      }
      nxtSeqNo++;
    }
    dupAckCnt=0;


    FD_ZERO(&fds);
    FD_CLR(*currentSockFd,&fds);
    FD_SET(*currentSockFd,&fds);
    val = select(*currentSockFd + 1, &fds, NULL, NULL, &timeoutInterval);
    if(val == 0) {
      ssthresh = (cwnd * segmentSize) / 2;
      cwnd = 1;
      timeoutInterval.tv_sec = 2 * timeoutInterval.tv_sec;
      timeoutInterval.tv_usec = 2 * timeoutInterval.tv_usec;
      continue;
    }
    if(val == -1) {
      log_error("There is some problem in receiving the segment!");
    }
    if(FD_ISSET(*currentSockFd, &fds) && val == 1) {
      ack = receiveAck(currentSockFd, otherEnd);
      gettimeofday(&t2, NULL);
      if(ack->ackNo < nxtSeqNo) {
        dupAckCnt++;
        while(dupAckCnt < 3) {
          ack = receiveAck(currentSockFd, otherEnd);
          dupAckCnt++;
        }
        for(int i = 0; i < noOfPacketsToDrop; i++) {
          if(ack->ackNo == packetsToDrop[i]) {
            packetsToDrop[i] =-1;
          }
        }
      }
      if(dupAckCnt < 3 && minimumSize == cwnd) {
        if((cwnd * segmentSize) >= ssthresh) {
          log_info("Congestion Avoidance phase ");
          cwnd = cwnd + 1;
          log_info("Next Sequence Number %d",ack->ackNo);
        } else {
          log_info("Slow Start");
          cwnd = cwnd * 2;
        }
        timeoutInterval = calculateTimeout(t1,t2);
      }
      firstUnAck = ack->ackNo;
      nxtSeqNo = ack->ackNo;
      free(ack);
    }
  }
  log_debug("Finished sending");
  free(udpSegments);
  FD_CLR(*currentSockFd,&fds);
  log_debug("Clearing fds");
}
