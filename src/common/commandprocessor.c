
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "commandprocessor.h"
#include "fileprocessor.h"
#include "udpsegment.h"
#include "log.h"
#include "transport.h"





char* executeCommand(char *);
char fullCOmmand[100];

char *globalFileName;
char *dir = ".\0";

char *firstPart;
char *secondPart;


void printCommandError() {
  printf("Error: Unable to understand the command\n");
}

void printFileCommandError() {
  printf("Error: FileName is required to complete this operation\n");
}

void splitCommand(char *input) {
  firstPart = strtok(input, " ");
  secondPart = strtok(NULL, " ");
}



bool isLocalLs() {
  if (strcmp(firstPart, "ls") == 0 || strcmp(firstPart, "LS") == 0) {
    if (secondPart == NULL) {
      secondPart = " .\0";
    }
    strcat(fullCOmmand,"find ");
    strcat(fullCOmmand, secondPart);
    return true;
  } else {
    return false;
  }
}

bool isRemoteLs() {

  if (strcmp(firstPart, "rls") == 0 || strcmp(firstPart, "RLS") == 0) {
    if (secondPart == NULL) {
      secondPart = " . \0";
    }
    strcat(fullCOmmand,"find ");
    strcat(fullCOmmand, secondPart);
    strcat(fullCOmmand, " ");
    return true;
  } else {
    return false;
  }
}

char * getFileOperation(char *input) {
  char *operation = strtok(input, " ");
  globalFileName = strtok(NULL, " ");
  log_debug("FileName %s", globalFileName);
  return operation;
}

bool isGet() {
  log_debug("In isGet function %s", firstPart);
  if (strcmp(firstPart, "get") == 0 || strcmp(firstPart, "GET") == 0) {
    strcat(fullCOmmand,"get ");
    if (secondPart != NULL) {
      strcat(fullCOmmand, secondPart);
      strcat(fullCOmmand, " ");
    }
    return true;
  } else {
    return false;
  }
}

bool isPut() {
  log_debug("In isPut function");
  if (strcmp(firstPart, "put") == 0 || strcmp(firstPart, "PUT") == 0) {
    strcat(fullCOmmand,"put ");
    if (secondPart != NULL) {
      strcat(fullCOmmand, secondPart);
      strcat(fullCOmmand, " ");
    }
    return true;
  } else {
    return false;
  }
}

bool isQuit() {
  log_debug("In isQuit function");
  if (strcmp(firstPart, "quit") == 0 || strcmp(firstPart, "QUIT") == 0) {
    return true;
  } else {
    return false;
  }
}
bool isHelp() {
  log_debug("In isHelp function");
  if (strcmp(firstPart, "help") == 0 || strcmp(firstPart, "help") == 0) {
    return true;
  } else {
    return false;
  }
}

void printHelp() {
  printf("You can execute the following commands. \n");
  printf("(1) help             - To display help\n");
  printf("(2) ls [directory]   - To list all the files and dir in the specified \n");
  printf("                       directory. If directory is not specified, then \n");
  printf("                       it will list out all the files and dir in \n");
  printf("                       the current directory\n");
  printf("(3) rls [directory]  - To list all the files and dir in the specified \n");
  printf("                       directory of the server machine. If directory \n");
  printf("                       is not specified, then it will list out all \n");
  printf("                       the files and dir in the current directory\n");
  printf("(4) put <FileName>   - Upload the specified file to the remote server\n");
  printf("(5) get <FileName>   - Download the specified file to the remote server\n");
}


void issueGetCommandAtTheServer(char *command, int *currentSockFd, struct sockaddr_in* otherEnd, int windowSize) {
    reliableSend(command, strlen(command), currentSockFd, otherEnd,  windowSize, COMMAND_OR_END, SUCCESS);
}

int processFileOperation(char op, int *currentSockFd, struct sockaddr_in* otherEnd, int windowSize) {
  log_debug("In processFileOperation function");
  if (secondPart == NULL) {
    printFileCommandError();
    return 0;
  } else {
    if (op == GET) {
      log_debug("Operation GET %s", secondPart);
      issueGetCommandAtTheServer(fullCOmmand, currentSockFd, otherEnd, windowSize);
    } else if (op == PUT) {
      log_debug("Operation GET %s", secondPart);
      if (exists(secondPart)) {
        issueGetCommandAtTheServer(fullCOmmand, currentSockFd, otherEnd, windowSize);
        char *fileData = getFileContentByFileName(secondPart);
        long fileSize = findSizeByFileName(secondPart);
        log_debug("File name : %s   read size = %llu", secondPart, fileSize);
        reliableSend(fileData, fileSize, currentSockFd, otherEnd,  windowSize, DATA, SUCCESS);
        printf("Uploaded %s successfully to the server\n", secondPart);
      } else {
        printf("File %s does not exist\n", secondPart);
      }
      return 0;
    }
  }
  return 1;
}

void processLocalLs(char *command, FILE *out) {
    char path[1035];
    FILE *fp = popen(command, "r");
    if (fp == NULL) {
      log_error("Failed to run command\n" );
      return;
    }
    while (fgets(path, sizeof(path), fp) != NULL) {
      fprintf(out, path);
    }
    pclose(fp);
}

void processRemoteLs(int *currentSockFd, struct sockaddr_in* otherEnd, int windowSize) {
  log_debug("Command \"%s\"", fullCOmmand);
  long fileSize = strlen(fullCOmmand);
  log_debug("Data length : %d", fileSize);
  log_debug("currentSockFd(%d) OtherEnd(%u) otherEndLen(%d)", *currentSockFd, otherEnd, sizeof(otherEnd));
  reliableSend(fullCOmmand, fileSize, currentSockFd, otherEnd,  windowSize, COMMAND_OR_END, SUCCESS);
}




int parseInput(char *input, int *currentSockFd, void *otherEnd, int windowSize) {
  bzero(fullCOmmand, 100);
  if (input != NULL)  {
    int commandLen = strlen(input);
    if (commandLen >= 2 ) {
      splitCommand(input);
      log_debug("Command first part : %s , second part: %s", firstPart, secondPart);
      if (isLocalLs()) {
        log_debug("Its a local command %s", fullCOmmand);
        processLocalLs(fullCOmmand, stdout);
      } else if (isRemoteLs()) {
        log_debug("Its a remote command");
        log_debug("currentSockFd(%d) OtherEnd(%u) otherEndLen(%d)", *currentSockFd, otherEnd, sizeof(*otherEnd));
        processRemoteLs(currentSockFd, (struct sockaddr_in*) otherEnd, windowSize);
        return 1;
      } else {
        if (isPut()) {
          log_debug("FileOperation PUT ");
          processFileOperation(PUT, currentSockFd, (struct sockaddr_in*) otherEnd, windowSize);
        } else if (isGet()) {
          log_debug("FileOperation GET ");
          if (processFileOperation(GET, currentSockFd, (struct sockaddr_in*) otherEnd, windowSize)) {
            Response *response = readResponse(windowSize, currentSockFd, otherEnd);
            if (response->length == 1 && response->receiveBuffer[0]->error) {
              printf("%s\n", response->receiveBuffer[0]->data);
            } else {
              log_debug("Received response size %d ", response->length);
              log_debug("Going to save file %s: ", secondPart);
              saveFile(secondPart, response);
            }
            free(response);
          }
        } else if (isQuit()) {
          printf("Good Bye!\n");
          exit(0);
        } else if (isHelp()) {
          printHelp();
        } else {
          printCommandError();
        }
      }
    } else {
      printCommandError();
    }
  } else {
    printCommandError();
  }
  return 0;
}
