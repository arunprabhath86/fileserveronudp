#include <sys/socket.h>
#include "udpsegment.h"

#ifndef COMMANDPROCESSOR_H
#define COMMANDPROCESSOR_H

#define GET 'g'
#define PUT 'p'

/**
 * This function parse the input from the user and
 * do appropriate actions. This function is being
 * used by the client, so that this function will parse
 * the command and do operations.
 * @param1 userInput
 * @param2 Address of the current socket fd (Client sockfd)
 * @param3 Address of the remote address (struct sockaddr_in)
 *         as void pointer.
 * @param4 Advertisement window size.
 * @return 0 or 1, if 1 returns then client will be ready to accept
 *         response from the server, otherwise client start receiving
 *         userInput.
 **/
int parseInput(char *, int* , void *, int);

#endif
