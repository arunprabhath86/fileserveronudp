#ifndef TRANSPORT_H
#define TRANSPORT_H

#include "udpsegment.h"

/**
 * This is the structure for storing the received data and its length.
 */
typedef struct {
  UDPSegment ** receiveBuffer;
  int length;
} Response;

/**
 * @param 1 - Data to be sent int *currentSockFd, int advWindowSize
 * @param 2 - Data size in bytes
 * @param 3 - Address of the current socket fd
 * @param 4 - Address of the remote address struct (struct sockaddr_in),
 *              should be passed as a void pointer
 * @param 5 - windowSize
 * @param 6 - Segment type @Refer udpsegment.h
 * @param 7 - error flag
 */
void reliableSend(char *, long, int *, struct sockaddr_in *, int, int, int);
/**
 * @param 1 - windowSize
 * @param 2 - Address of the current socket fd
 * @param 3 - Address of the remote address struct (struct sockaddr_in)
 * @return address of the Response
 */
Response *readResponse(int , int *, struct sockaddr_in *);



#endif
