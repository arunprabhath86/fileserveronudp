#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "../common/log.h"
#include "../common/udpsegment.h"
#include "../common/fileprocessor.h"
#include "../common/commandprocessor.h"
#include "../common/transport.h"

#define PORT 5000
#define MAXLINE 1024

int udpPort;
int advWindowSize;
int serSocket;
int logLevel = 5;
struct sockaddr_in serSockAddr;
socklen_t clientSockLen;
UDPSegment *segment;

char *firstPart;
char *secondPart;

/**
 * This function will display the usage of the server
 */
void printUsage() {
  printf ("Usage: rudpserver -p <portNo> -w <advertised windowSize> [-l <logLevel>]\n");
  exit(0);
}

/**
 * This function processes the argument and if it finds any error
  * while processing, it will show the usage and exit the
  * application.
 */
void processCommandLineOptions(int argc, char **argv) {
  if (argc < 4) {
    printUsage();
  }
  int options;
  while ((options = getopt(argc, argv, "p:w:l:")) != -1) {
    switch (options) {
      case 'p':
        udpPort = atoi(optarg);
        break;
      case 'w':
        advWindowSize = atoi(optarg);
        break;
      case 'l':
        logLevel = atoi(optarg);
        log_set_level(logLevel);
        break;
      default:
        printUsage();
    }
  }
  log_info("Argument received: UDP Port(%d), Advertised Widow Size (%d) and logLevel(%d)",
                                  udpPort, advWindowSize, logLevel);
}

/**
 * Creating udp socket and binding to the udp port specified in the
 * command line argument.
 */
void createSocketAndBindAddress() {
	serSocket = socket(AF_INET, SOCK_DGRAM, 0);
  if (serSocket < 0)	{
		log_error("Something went wrong while creating the server socket");
    exit(0);
	}
  log_debug("Server socke fd %d", serSocket);
  int leng = sizeof(serSockAddr);
	bzero(&serSockAddr, leng);
	serSockAddr.sin_family = AF_INET;
	serSockAddr.sin_port = htons(udpPort);
	serSockAddr.sin_addr.s_addr = INADDR_ANY;
	if (bind(serSocket, (struct sockaddr *) &serSockAddr, leng) < 0) {
		log_error("Something went wrong while binding the server socket to an address!");
    exit(0);
	}
  log_info("Server starts listening to the UDP port %d. Now it is ready to accept client request", udpPort);
}

char* executeCommand_(char command[]) {
  FILE *fp;
  char output[20*1035];
  char path[1035];
  bzero(output, 20*1035);
  bzero(path, 1035);
  fp = popen(command, "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    exit(1);
  }

  while (fgets(path, sizeof(path), fp) != NULL) {
    strcat(output, path);
    bzero(path, 1035);
  }
  pclose(fp);
  return output;
}

bool isFind() {

  if (strcmp(firstPart, "find") == 0 || strcmp(firstPart, "FIND") == 0) {
    log_debug("In isFind compre finsh");
    return true;
  } else {
    return false;
  }
}
bool isGetReceived() {
  if (strcmp(firstPart, "get") == 0 || strcmp(firstPart, "GET") == 0) {
    return true;
  } else {
    return false;
  }
}

bool isPutRecieved() {
  if (strcmp(firstPart, "put") == 0 || strcmp(firstPart, "PUT") == 0) {
    return true;
  } else {
    return false;
  }
}

void splitServerCommand(char *input,  char *splitBy) {
  firstPart = strtok(input, splitBy);
  secondPart = strtok(NULL, splitBy);
}

void acceptClientRequest() {
  while (1) {
    Response *response = readResponse(advWindowSize, &serSocket, &serSockAddr);
    if (response->length == 1 && response->receiveBuffer[0]->flag ==  COMMAND_OR_END) {
      log_debug("'%s' command received  with flag : %d ", response->receiveBuffer[0]->data, response->receiveBuffer[0]->flag);
      char *command = response->receiveBuffer[0]->data;
      splitServerCommand(command, " ");
      log_debug("Split complete");
      log_debug("firstPart: %s , secondPart: %s ", firstPart, secondPart);
      if (isFind()) {
        char originalCommand[100];
        bzero(originalCommand, 100);
        strcat(originalCommand, firstPart);
        log_debug("Concat %s", originalCommand);
        strcat(originalCommand, " ");
        strcat(originalCommand, secondPart);
        log_debug("Original Command :%s", originalCommand);
        char *output = executeCommand_(originalCommand);
        log_debug("%s", output);
        reliableSend(output, strlen(output), &serSocket, &serSockAddr,  advWindowSize, DATA, SUCCESS);
      } else if (isGetReceived()) {
         if (exists(secondPart)) {
           char *fileData = getFileContentByFileName(secondPart);
           long fileSize = findSizeByFileName(secondPart);
           reliableSend(fileData, fileSize, &serSocket, &serSockAddr,  advWindowSize, DATA, SUCCESS);
         } else {
           char msg[100];
           sprintf(msg, "Cannot find %s on server", secondPart);
           log_debug("Cannot find %s on server so sending errro(%d)", secondPart, ERROR);
           reliableSend(msg, strlen(msg), &serSocket, &serSockAddr,  advWindowSize, COMMAND_OR_END, ERROR);
         }
      } else if (isPutRecieved()) {
        Response *fileData = readResponse(advWindowSize, &serSocket, &serSockAddr);
        saveFile(secondPart, fileData);
        free(fileData);
      }
    }
    free(response->receiveBuffer);
    free(response);
  }
}

/**
 *
 */
int main(int argc, char **argv) {
  processCommandLineOptions(argc, argv);
  createSocketAndBindAddress();
  acceptClientRequest();

}
