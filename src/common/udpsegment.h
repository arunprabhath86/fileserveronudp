#ifndef UDPSEGMENT_H
#define UDPSEGMENT_H

#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#define MSS 1450

#define COMMAND_OR_END 0 // Command segment flag
#define DATA 1 // Data segment flag
#define ACK 2 // Ack segment flag
#define FILE_HEADER 3  // File header flag
#define ERROR 1
#define SUCCESS 0

typedef struct udpsegment {
  /* This is the 32-bit sequence number of the segment that is to be sent */
  uint32_t seqNo;

  /** This is the 32-bit acknowledgement number for the correctly received
   * segment
   */
  uint32_t ackNo;
  /**
   * If ackFlag is set, then it means an acknowledgment packet.
   */
  uint32_t flag;

  uint32_t error;

  char data[MSS];

} UDPSegment;


char *toString(UDPSegment);

#endif
