#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <getopt.h>
#include <netdb.h>
#include <errno.h>

#include "../common/log.h"
#include "../common/commandprocessor.h"
#include "../common/transport.h"

#define PORT 5000
#define MAXLINE 1024

int udpPort;
char *hostName;
int clientSocket;
int windowSize = 1450;
struct hostent *host;
int logLevel = 5;

struct sockaddr_in serverAddress, clientAddress;


void setServerAddress(int portNo) {
	bzero((char *) &serverAddress, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	bcopy((char *)host->h_addr, (char *)&serverAddress.sin_addr.s_addr, host->h_length);
	serverAddress.sin_port = htons(portNo);
	log_debug("serverAddress : %u", &serverAddress);
}

void getServerInfo(char* hostname) {
	host = gethostbyname(hostname);
	if(host==NULL) {
		log_error("There is no such host!");
	}
}


void createSocket() {
	clientSocket=socket(AF_INET,SOCK_DGRAM,0);
	log_debug("clientSocket : %d", clientSocket);
	if (clientSocket < 0) {
		log_error("The Socket cannot be opened");
	}
}


void printUsage() {
  printf ("Usage: urdpclient -h <hostname> -p <protNo> [-l<logLevel>]\n");
  exit(0);
}

void processCommandLineOptions(int argc, char **argv) {
  if (argc < 4) {
    printUsage();
  }
  int options;
  while ((options = getopt(argc, argv, "h:p:l:w:")) != -1) {
    switch (options) {
      case 'p':
        udpPort = atoi(optarg);
        break;
      case 'h':
        hostName = optarg;
        break;
			case 'w':
        windowSize = atoi(optarg);
        break;
			case 'l':
	       logLevel = atoi(optarg);
				 log_set_level(logLevel);
	       break;
      default:
        printUsage();
    }
  }
  log_info("Argument received: Host (%s), UDP Port (%d) windowSize (%d) and logLevel(%d)", hostName,
																			udpPort, windowSize, logLevel);
}

void printResponse(Response *response) {
	if (response) {
		for (int i = 0; i < response->length; i++) {
			printf("%s", response->receiveBuffer[i]->data);
		}
	}
}

int main(int argc, char **argv) {
  char input[50];
    processCommandLineOptions(argc, argv);
		log_set_level(logLevel);
    getServerInfo(hostName);
    createSocket();
    setServerAddress(udpPort);
		int otherSockLength = sizeof(serverAddress);
		log_debug("otherSockLength %d at %u", otherSockLength, &otherSockLength);
    while (1) {
      bzero((char *) &input, sizeof(char) * 50);
      printf("FTPU>> ");
      fgets(input, 50, stdin);
			input[strcspn(input, "\r\n")] = 0;
			log_debug("currentSockFd(%d) OtherEnd(%u) otherEndLen(%d)", clientSocket,
								&serverAddress, sizeof(serverAddress));
			log_debug("Command received: %s", input);
      if(parseInput(input, &clientSocket, (void *)&serverAddress, windowSize)) {
				Response *response = readResponse(windowSize, &clientSocket, &serverAddress);
				printResponse(response);
				free(response);
			}
    }
    return 0;
}
