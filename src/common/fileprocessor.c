
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


#include "fileprocessor.h"
#include "log.h"
#include "transport.h"
#include "commandprocessor.h"

char *filePart1;
char *filePart2;

void processFile() {

}



long readContent(char *content, FILE *fp, long fileSize) {
  if (fp == NULL) {
    log_error("File Not Found!");
    return -1;
  }
  size_t numRead = fread(content, sizeof(char), fileSize, fp);
  content[numRead++] = '\0';
  return numRead;
}

bool exists(const char *fname) {

    if(access(fname, F_OK) != -1 ) {
      return true;
    } else {
      return false;
    }
}

splitFileName(char *fileName, char*splitBy) {
  log_debug("splitFileName fileName: %s spliyBy: %s", fileName, splitBy);
  filePart1 = strtok(fileName, splitBy);
  filePart2 = strtok(NULL, splitBy);
  log_debug("FilePart1: %s FilePart2: %s", filePart1, filePart2);
}

void generateFileName(char *fileName, int count, char *out) {
  char copyFileName[100];
  strcpy(copyFileName, fileName);
  log_debug("Generating new filename using %s and count %d", fileName, count);
  char outputFileName[100];
  char snum[5];
  sprintf(snum, "%d", count);
  bzero(outputFileName, 100);
  splitFileName(copyFileName, ".");
  strcat(outputFileName, filePart1);
  strcat(outputFileName, "(");
  strcat(outputFileName, snum);
  strcat(outputFileName, ")");
  if (filePart2 != NULL) {
    strcat(outputFileName, ".");
    strcat(outputFileName, filePart2);
  }
  log_debug("FileName generated : %s its length %d", outputFileName, strlen(outputFileName));
  strcpy(out, outputFileName);
}

bool convertoUtf8(unsigned char *in, unsigned char *out) {
  while (*in)
    if (*in<128) *out++=*in++;
    else *out++=0xc2+(*in>0xbf), *out++=(*in++&0x3f)+0x80;
}

void saveByCheckingFileName(char *fileName, Response *data) {
    log_debug("In saveByCheckingFileName: filename :%s", fileName);
    int count = 0;
    char out[100];
    bzero(out, 100);
    strcpy(out, fileName);
    while (exists(out)) {
      log_debug("Filename %s exists and initialFileName %s", out, fileName);
      count++;
      generateFileName(fileName, count, out);
      log_debug("New filename %s", out);
    }

    char utf8Filename[50];
    bzero(utf8Filename, 50);
    convertoUtf8(out, utf8Filename);
    log_debug("Going to save the file as %s", utf8Filename);
    char *fileData;
    FILE *fp;
    fp  = fopen (utf8Filename, "wb");
    if (fp > 0 ) {
      int totalBytesWritten = 0;
      for (int i = 0; i < data->length; i++) {
        totalBytesWritten+= fwrite(data->receiveBuffer[i]->data, 1,
          strlen(data->receiveBuffer[i]->data), fp);
          fflush(fp);
      }
      fclose(fp);
      printf("File %s saved successfully (%d) bytes\n", utf8Filename, totalBytesWritten);
    } else {
      log_debug("Cannot create file %s Error: %s", utf8Filename, strerror(errno));
    }
}

void saveFile(char *fileName, Response *data) {
  int len = 0;
  int size = 0;
  for (int i = 0; i < data->length; i++) {
    len+=strlen(data->receiveBuffer[i]->data);
    size+=sizeof(data->receiveBuffer[i]->data);
  }
    log_debug("In Save file: filename :%s and len: %d , size: %d", fileName, len, size);
    saveByCheckingFileName(fileName, data);
}





/**
 * This function read the content of the file and
 * return it.
 */
char *getFileContentByFileName(char fileName[]) {
  long fileSize = findSizeByFileName(fileName);
  log_debug("File Size %llu bytes", fileSize);

  char *content = malloc(sizeof(char) * (fileSize + 1));
  FILE *fp = fopen(fileName, "r");
  long numBytesRead = readContent(content, fp, fileSize);
  log_debug("%llu bytes read from the file : %s", numBytesRead, fileName);
  fclose(fp);
  return content;
}

char *getFileContentByFilePointer(FILE *fp) {
  long fileSize = findSizeByFilePointer(fp);
  char *content = malloc(sizeof(char) * (fileSize + 1));
  readContent(content, fp, fileSize);
  return content;
}






long findSizeByFilePointer(FILE* fp) {
  if (fp == NULL) {
      log_error("File Not Found!");
      return -1;
  }
  fseek(fp, 0L, SEEK_END);
  long int res = ftell(fp);
  rewind(fp);
  return res;
}


/**
 * This function returns the size of the file.
 */
long findSizeByFileName(char file_name[]) {
    FILE* fp = fopen(file_name, "r");
    if (fp == NULL) {
        log_error("File Not Found!");
        return -1;
    }
    fseek(fp, 0L, SEEK_END);
    long int res = ftell(fp);
    fclose(fp);
    return res;
}
